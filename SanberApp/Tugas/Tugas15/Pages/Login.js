import React from 'react'
import { Button, StyleSheet, Text, View } from 'react-native'

export default function Login({ navigation }) {
    return (
        <View style={styles.container}>
            <Text>Google Login</Text>
            <Button
                onPress={() => navigation.navigate('MyDrawer', {
                    screen: 'App', params: {
                        screen: 'HomeScreen'
                    }
                })}
                title="Continue with Google Account"
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})
