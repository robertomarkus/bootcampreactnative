import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Ionicons } from '@expo/vector-icons';
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createDrawerNavigator } from '@react-navigation/drawer'
import { NavigationContainer } from '@react-navigation/native'

import Login from '../Pages/Login'
import Home from '../Pages/Home'
import AddScreen from '../Pages/AddScreen'
import About from '../Pages/About'
import ProjectScreen from '../Pages/ProjectScreen'
import Setting from '../Pages/Setting'
import SkillProject from '../Pages/SkillProject'
import LoginScreen from '../../Tugas13/LoginScreen'
import AboutScreen from '../../Tugas13/AboutScreen'
import Telegram from '../../Tugas12/Telegram';
import RestApi from '../../Tugas14/RestApi';

const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

export default function Router() {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name="LoginScreen" component={LoginScreen} />
                <Stack.Screen name="Login" component={Login} />
                <Stack.Screen name="HomeScreen" component={Home} />
                <Stack.Screen name="MainApp" component={MainApp} />
                <Stack.Screen name="MyDrawer" component={MyDrawer} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

const MainApp = () => (
    <Tab.Navigator
        screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
                let iconName;

                if (route.name === 'HomeScreen') {
                    iconName = focused ? 'md-home' : 'md-home-outline';
                } else if (route.name === 'About') {
                    iconName = focused ? 'md-information-circle' : 'md-information-circle-outline';
                } else if (route.name === 'Profile') {
                    iconName = focused ? 'md-person-circle' : 'md-person-circle-outline';
                } else if (route.name === 'ProjectScreen') {
                    iconName = focused ? 'md-list-circle' : 'md-list-circle-outline';
                } else if (route.name === 'SkillProject') {
                    iconName = focused ? 'md-code-slash' : 'md-code-slash-outline';
                } else if (route.name === 'Chat') {
                    iconName = focused ? 'md-chatbubbles' : 'md-chatbubbles-outline';
                }


                // You can return any component that you like here!
                return <Ionicons name={iconName} size={size} color={color} />;
            },
        })}
        tabBarOptions={{
            activeTintColor: 'darkblue',
            inactiveTintColor: 'gray',
        }}
    >
        <Tab.Screen name="HomeScreen" component={Home} />
        <Tab.Screen name="Chat" component={Telegram} />
        <Tab.Screen name="Profile" component={AboutScreen} />
        <Tab.Screen name="ProjectScreen" component={ProjectScreen} />
        <Tab.Screen name="SkillProject" component={SkillProject} />
    </Tab.Navigator>
)

const MyDrawer = () => (
    <Drawer.Navigator>
        <Drawer.Screen name="App" component={MainApp} />
        <Drawer.Screen name="News" component={RestApi} />
        <Drawer.Screen name="AddScreen" component={AddScreen} />
        <Drawer.Screen name="SettingScreen" component={Setting} />
        <Drawer.Screen name="About" component={About} />
    </Drawer.Navigator>
)