import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { ScrollView } from 'react-native-gesture-handler';

export default function AboutScreen() {
    return (
        <View style={styles.container}>
            <ScrollView>
                <View style={styles.mainDescription}>
                    <Text style={styles.pageTitle}>About Me</Text>
                    <Ionicons name="person-circle-outline" size={180} color="black" />
                    <Text style={{ fontWeight: 'bold', fontSize: 24, borderBottomWidth: 1, borderBottomColor: 'black' }}>Markus Roberto</Text>
                    <Text style={{ fontWeight: 'bold', fontSize: 16 }}>React Native Developer</Text>
                </View>
                <View style={styles.activities}>
                    <TouchableOpacity style={styles.listActivities}>
                        <Text style={{ fontWeight: 'bold', fontSize: 16, color: 'darkblue' }}>22</Text>
                        <Text style={{ fontSize: 12, color: 'darkblue' }}>Certificates</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.listActivities}>
                        <Text style={{ fontWeight: 'bold', fontSize: 16, color: 'darkblue' }}>17</Text>
                        <Text style={{ fontSize: 12, color: 'darkblue' }}>Experiences</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.listActivities}>
                        <Text style={{ fontWeight: 'bold', fontSize: 16, color: 'darkblue' }}>28</Text>
                        <Text style={{ fontSize: 12, color: 'darkblue' }}>Projects</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.listActivities}>
                        <Text style={{ fontWeight: 'bold', fontSize: 16, color: 'darkblue' }}>69</Text>
                        <Text style={{ fontSize: 12, color: 'darkblue' }}>Connections</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.mainDescription}>
                    <TouchableOpacity style={styles.button}>
                        <Text style={{ fontWeight: 'bold', color: 'white' }}>Skills Summary</Text>
                    </TouchableOpacity>
                </View>
                <View style={{ paddingHorizontal: 45, paddingVertical: 10 }}>
                    <Text style={{ fontWeight: 'bold', fontSize: 16, color: 'black' }}>External Portfolio</Text>
                    <View style={styles.contentBox}>
                        <TouchableOpacity style={styles.boxItem}>
                            <Ionicons name="logo-bitbucket" size={30} color="black" />
                            <Text style={{ fontSize: 10, fontWeight: 'bold', color: 'blue' }}>@bitbucket</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.boxItem}>
                            <Ionicons name="logo-github" size={30} color="black" />
                            <Text style={{ fontSize: 10, fontWeight: 'bold', color: 'blue' }}>@github</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.boxItem}>
                            <Ionicons name="logo-dribbble" size={30} color="black" />
                            <Text style={{ fontSize: 10, fontWeight: 'bold', color: 'blue' }}>@dribbble</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.boxItem}>
                            <Ionicons name="logo-instagram" size={30} color="black" />
                            <Text style={{ fontSize: 10, fontWeight: 'bold', color: 'blue' }}>@instagram</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ paddingHorizontal: 45, paddingVertical: 10 }}>
                    <Text style={{ fontWeight: 'bold', fontSize: 16, color: 'black' }}>Contact</Text>
                    <View style={styles.contentBox}>
                        <TouchableOpacity style={styles.boxItem}>
                            <Ionicons name="logo-linkedin" size={30} color="black" />
                            <Text style={{ fontSize: 8, fontWeight: 'bold', color: 'blue' }}>@linkedin</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.boxItem}>
                            <Ionicons name="mail-outline" size={30} color="black" />
                            <Text style={{ fontSize: 8, fontWeight: 'bold', color: 'blue' }}>google@gmail.com</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.boxItem}>
                            <Ionicons name="logo-whatsapp" size={30} color="black" />
                            <Text style={{ fontSize: 8, fontWeight: 'bold', color: 'blue' }}>+19483458457</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.boxItem}>
                            <Ionicons name="logo-facebook" size={30} color="black" />
                            <Text style={{ fontSize: 8, fontWeight: 'bold', color: 'blue' }}>@facebook</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    pageTitle: {
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: 30,
        fontSize: 25,
        fontWeight: 'bold'
    },
    mainDescription: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    activities: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 10
    },
    listActivities: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 5,
    },
    button: {
        flexDirection: 'row',
        backgroundColor: 'darkblue',
        alignItems: 'center',
        justifyContent: 'center',
        height: 40,
        width: 160,
        borderRadius: 20,
        marginLeft: 50,
        marginRight: 50,
        marginTop: 10,
        marginBottom: 10
    },
    contentBox: {
        flexDirection: 'row',
        backgroundColor: 'lightgray',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 10,
        paddingHorizontal: 10,
        borderWidth: 1,
        borderColor: 'black'
    },
    boxItem: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 5,
    }
})