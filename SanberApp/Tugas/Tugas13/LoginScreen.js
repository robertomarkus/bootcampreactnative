import React from 'react';
import { Image, StyleSheet, View, Text, TextInput, Button, TouchableHighlight, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

export default function LoginScreen({ navigation }) {
    return (
        <View style={styles.container}>
            <Image
                style={styles.imageLogo}
                source={require('./assets/logo.png')}
            />
            <Text style={styles.pageTitle}>LOGIN</Text>
            <View style={styles.form}>
                <Text style={styles.formTitle}>Username / Email</Text>
                <TextInput
                    style={{ borderColor: 'blue', borderWidth: 1, borderRadius: 10, marginBottom: 15 }}
                    placeholder="Enter your username or email" />
                <Text style={styles.formTitle}>Password</Text>
                <TextInput
                    style={{ borderColor: 'blue', borderWidth: 1, borderRadius: 10, marginBottom: 15 }}
                    placeholder="Enter your password" />
            </View>
            <View style={styles.actionZone}>
                <TouchableOpacity
                    style={styles.button}
                    onPressIn={() => navigation.navigate('MyDrawer', {
                        screen: 'App', params: {
                            screen: 'HomeScreen'
                        }
                    })}
                >
                    <Text style={{ fontWeight: 'bold', color: 'white' }}>Login</Text>
                </TouchableOpacity>
                <Text>Or Continue With</Text>
                <TouchableOpacity
                    style={styles.buttonGoogle}
                    onPressIn={() => navigation.navigate('Login')}
                >
                    <Ionicons name="md-logo-google" size={30} color="black" />
                    <Text style={{ fontWeight: 'bold', color: 'black' }}>Google</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    imageLogo: {
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: 35,
        height: 78,
        width: 290
    },
    pageTitle: {
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: 65,
        fontSize: 25,
        fontWeight: 'bold'
    },
    form: {
        flexDirection: 'column',
        paddingHorizontal: 24,
        paddingVertical: 24,
    },
    formTitle: {
        alignSelf: 'flex-start',
    },
    actionZone: {
        flexDirection: 'column',
        alignItems: 'center',
        paddingHorizontal: 24,
    },
    button: {
        flexDirection: 'row',
        backgroundColor: 'darkblue',
        alignItems: 'center',
        justifyContent: 'center',
        height: 40,
        width: 160,
        borderRadius: 20,
        marginLeft: 50,
        marginRight: 50,
        marginTop: 10,
        marginBottom: 10
    },
    buttonGoogle: {
        flexDirection: 'row',
        backgroundColor: 'lightblue',
        alignItems: 'center',
        justifyContent: 'center',
        height: 40,
        width: 160,
        borderRadius: 20,
        marginLeft: 50,
        marginRight: 50,
        marginTop: 10,
        marginBottom: 10
    }
})