import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, ScrollView, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class Login extends Component {
  state = {
    dataID: [],
    dataProvinsi: []
  }

  getIndonesiaData() {
    fetch('https://api.kawalcorona.com/indonesia')
      .then(res => res.json())
      .then(res => {
        this.setState({ dataID: res[0] })
      })
  }
  getProvinceData() {
    fetch('https://api.kawalcorona.com/indonesia/provinsi')
      .then(res => res.json())
      .then(res => {
        this.setState({ dataProvinsi: res })
      })
  }

  componentDidMount() {
    this.getIndonesiaData();
    this.getProvinceData();
  }

  render() {
    const { navigate } = this.props.navigation;
    const { dataID } = this.state
    const provinsi = this.state.dataProvinsi.slice(0, 34).map(data => {
      return <TouchableOpacity
        key={data.attributes.Kode_Provi}
        style={styles.dataCovid}
        onPress={() => navigate('Detail',
          { provinsi: data.attributes })}
      >
        <Text
          style={{ fontWeight: 'bold', fontSize: 14 }}
        >{data.attributes.Provinsi}
        </Text>
      </TouchableOpacity>
    })
    return (
      <View style={styles.container}>
        <ScrollView style={styles.body}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text style={{ fontWeight: 'bold', marginTop: 25, fontSize: 12 }}>Selamat Datang! {this.props.route.params.username}</Text>
            <TouchableOpacity>
              <Text
                style={{ fontWeight: 'bold', marginTop: 25, fontSize: 12 }}
                onPress={() => navigate('Login')}
              ><Icon name='logout' size={18} /></Text>
            </TouchableOpacity>
          </View>
          <View style={styles.jumbotron}>
            <Image source={require('./images/dokter2.png')}
              style={{ width: 100, height: 100, marginTop: 20 }}
            />
            <View style={{ width: 190, height: 60, marginTop: 20, marginBottom: 30 }}>
              <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 18, marginBottom: 5 }}>Tips Covid-19 <Icon name='lightbulb' size={18} /></Text>
              <Text style={{ color: '#fff', fontWeight: 'bold' }}>1. Selalu mencuci tangan!</Text>
              <Text style={{ color: '#fff', fontWeight: 'bold' }}>2. Hindari Menyentuh Wajah!</Text>
              <Text style={{ color: '#fff', fontWeight: 'bold' }}>3. Kenakan Masker Medis!</Text>
            </View>
          </View>
          <View style={styles.jumbotron}>            
            <View style={{ width: 150, height: 60, marginTop: 20, marginBottom: 30 }}>
              <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 18, marginBottom: 5 }}>Tips Kesehatan <Icon name='favorite' size={18} /></Text>
              <Text style={{ color: '#fff', fontWeight: 'bold' }}>1. Jaga Pola Tidur!</Text>
              <Text style={{ color: '#fff', fontWeight: 'bold' }}>2. Makan yang Bergizi!</Text>
              <Text style={{ color: '#fff', fontWeight: 'bold' }}>3. Berolahraga Teratur!</Text>
            </View>
            <Image source={require('./images/dokter1.png')}
              style={{ width: 80, height: 100, marginTop: 20 }}
            />
          </View>
          <View style={{ marginTop: 20 }}>
            <Text style={{ color: '#4f4f4f', fontWeight: 'bold', fontSize: 20, textAlign: 'center' }}>Total Kasus Covid-19 di Indonesia</Text>
            <View style={styles.dataContainer}>
              <View style={styles.dataCovid}>
                <Text style={{ fontWeight: 'bold', fontSize: 18, color: '#FF0202' }}>Positif</Text>
                <Text style={{ marginTop: 10, fontWeight: 'bold', fontSize: 18, color: '#FF0202' }}>{dataID.positif}</Text>
              </View>
              <View style={styles.dataCovid}>
                <Text style={{ fontWeight: 'bold', fontSize: 18, color: '#00D789' }}>Sembuh</Text>
                <Text style={{ marginTop: 10, fontWeight: 'bold', fontSize: 18, color: '#00D789' }}>{dataID.sembuh}</Text>
              </View>
              <View style={styles.dataCovid}>
                <Text style={{ fontWeight: 'bold', fontSize: 18, color: '#e6cb1e' }}>Dirawat</Text>
                <Text style={{ marginTop: 10, fontWeight: 'bold', fontSize: 18, color: '#e6cb1e' }}>{dataID.dirawat}</Text>
              </View>
              <View style={styles.dataCovid}>
                <Text style={{ fontWeight: 'bold', fontSize: 18, color: '#797979' }}>Meninggal</Text>
                <Text style={{ marginTop: 10, fontWeight: 'bold', fontSize: 18, color: '#797979' }}>{dataID.meninggal}</Text>
              </View>
            </View>
          </View>
          <View style={{ marginTop: 20 }}>
            <Text style={{ color: '#4f4f4f', fontWeight: 'bold', fontSize: 20, textAlign: 'center' }}>Data pada setiap Provinsi</Text>
            <View style={styles.dataContainer}>
              {provinsi}
            </View>
          </View>
        </ScrollView>
        <View style={styles.tabBar}>
          <TouchableOpacity
            style={styles.tabItem}
            onPress={() => navigate('Home')}>
            <Icon name='home' size={25} style={{ color: '#ffff'}}/>
            <Text style={styles.tabTitle}>Beranda</Text>
          </TouchableOpacity>
          <TouchableOpacity 
            style={styles.tabItem}
            onPress={() => navigate('News')}>
            <Icon name='assignment' size={25} style={{ color: '#ffff'}}/>
            <Text style={styles.tabTitle}>Tips</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.tabItem}
            onPress={() => navigate('About')}>
            <Icon name='list-alt' size={25} style={{ color: '#ffff', transform: [{ rotate: '180deg' }] }}/>
            <Text style={styles.tabTitle}>Informasi</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.tabItem}
            onPress={() => navigate('Profile')}>
            <Icon name='miscellaneous-services' size={25} style={{ color: '#ffff'}}/>
            <Text style={styles.tabTitle}>Pengaturan</Text>
          </TouchableOpacity>
        </View>
      </View>



    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffff',
    textAlign: 'center'
  },
  body: {
    margin: 10,
  },
  jumbotron: {
    backgroundColor: '#00ebab',
    height: 120,
    borderRadius: 15,
    marginTop: 13,
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  dataContainer: {
    marginTop: 15,
    borderColor: '#4f4f4f',
    justifyContent: 'space-around',
    flexDirection: 'row',
    flex: 1,
    flexWrap: 'wrap',
    textAlign: 'center'
  },
  dataCovid: {
    width: 160,
    height: 80,
    borderRadius: 15,
    borderWidth: 0,
    borderColor: '#4f4f4f',
    justifyContent: 'center',
    textAlign: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    elevation: 3,
    marginBottom: 10,
  },
   tabBar: {
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: '#00ebab',
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-around'
  }, tabItem: {
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    color: '#858ff585'
  }, tabTitle: {
    fontSize: 11,
    color: '#ffff',
    paddingTop: 4
  }
})


