import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class Login extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.navBar}>
                    {/* <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                        <Image source={require('./images/arrow_back.png')} style={{ width: 22, height: 22 }} />
                    </TouchableOpacity> */}
                    <Text style={styles.textNavbar}>Tentang Aplikasi</Text>
                </View>

                <View style={styles.body}>
                    <View>
                        <Image source={require('./images/statistics.png')} style={{ width: 200, height: 200 }} />
                    </View>
                    <Text style={styles.aboutApp}>
                        Aplikasi berikut merupakan suatu media informasi yang diciptakan untuk memantau kondisi seputar pademi virus covid-19 di tanah air.{"\n"}{"\n"}
                        Aplikasi ini juga diciptakan untuk memenuhi kebutuhan Tugas Akhir dari pelatihan Sanbercode - React Native.{"\n"}{"\n"}
                        Data referensi diambil dari : <Text style={{fontStyle: 'italic', color: 'cyan'}}>https://kawalcorona.com/api/</Text>
                    </Text>
                </View>
                <View style={styles.tabBar}>
                    <TouchableOpacity
                        style={styles.tabItem}
                        onPress={() => this.props.navigation.navigate('Home')}>
                        <Icon name='home' size={25}  style={{ color: '#ffff'}}/>
                        <Text style={styles.tabTitle}>Beranda</Text>
                    </TouchableOpacity>
                    <TouchableOpacity 
                    style={styles.tabItem}
                    onPress={() => this.props.navigation.navigate('News')}>
                        <Icon name='article' size={25}  style={{ color: '#ffff'}}/>
                        <Text style={styles.tabTitle}>Tips</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.tabItem}
                        onPress={() => this.props.navigation.navigate('About')}>
                       <Icon name='list-alt' size={25} style={{ color: '#ffff', transform: [{ rotate: '180deg' }] }}/>
                        <Text style={styles.tabTitle}>Informasi</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.tabItem}
                        onPress={() => this.props.navigation.navigate('Profile')}>
                        <Icon name='miscellaneous-services' size={25} style={{ color: '#ffff'}}/>
                        <Text style={styles.tabTitle}>Pengaturan</Text>
                    </TouchableOpacity>
                </View>
            </View>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffff'
    },
    navBar: {
        height: 84,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        backgroundColor: '#00ebab',
        elevation: 3,
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center',
    },
    textNavbar: {
        color: '#ffff',
        marginLeft: 60,
        fontWeight: 'bold',
        fontSize: 18,
        paddingLeft: 40
    },
    body: {
        flex: 1,
        marginTop: 20,
        alignItems: 'center',
        textAlign: 'center'
    },
    aboutApp: {
        fontSize: 15,
        margin: 20,
        padding: 24,
        backgroundColor: '#00ebab',
        color: '#fff',
        borderRadius: 15,
        textAlign: 'justify'
    },
    tabBar: {
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: '#00ebab',
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-around'
  }, tabItem: {
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    color: '#858ff585'
  }, tabTitle: {
    fontSize: 11,
    color: '#ffff',
    paddingTop: 4
  }
})


