import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TextInput, Button } from 'react-native';

export default class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            username: "",
            password: "",
            isError: false,
        }
    }

    loginHandler() {
        if (this.state.password === '12321') {
            this.props.navigation.navigate('Home',
                {
                    username: this.state.username
                });
            this.setState({ isError: false })
        } else {
            this.setState({ isError: true })
        }
    }

    render() {
        return (
            <View style={{ backgroundColor: '#00ebab', width: '100%', height: '100%' }}>
                <View style={styles.container}>
                    <View style={styles.logo}>
                        <Image source={require('./images/logo.png')} style={{ width: 290, height: 100 }} />
                    </View>
                    <View style={styles.loginSentence}>
                        <Text style={{ fontWeight: 'bold', color: '#fff', textAlign: 'center' }}>Portal Informasi Pandemi</Text>
                        <Text style={{ fontWeight: 'bold', color: '#fff', textAlign: 'center' }}>Covid-19 se Indonesia</Text>
                    </View>
                    <View>
                        <View>
                            <TextInput
                                type="text"
                                placeholder="Username"
                                style={styles.input}
                                value={this.state.username}
                                onChangeText={(username) => this.setState({ username })} />
                            <TextInput
                                secureTextEntry={true}
                                placeholder="Password"
                                style={styles.input}
                                value={this.state.password}
                                onChangeText={(password) => this.setState({ password })}
                                secureTextEntry={true}
                            />
                            <Text style={styles.forgetPassword}>Lupa password?</Text>
                        </View>
                        <Text
                            style={this.state.isError ? styles.errorText : styles.hiddenErrorText}>
                            Password Salah</Text>
                        <View style={styles.tombol}>
                            <Text
                                style={styles.tombolText}
                                onPress={() => this.loginHandler()}>
                                Masuk</Text>
                        </View>
                        {/* <Text style={{ marginTop: 20, color: '#fff', textAlign: 'center', fontSize: 18, textDecorationLine: 'underline' }}>Buat Akun Baru</Text> */}
                    <Text style={styles.newAccount}>Buat akun baru</Text>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        margin: 32
    },
    logo: {
        alignItems: 'center',
        marginBottom: 30
    },
    loginSentence: {
        textAlign: 'center',
        marginBottom: 20,
    },
    forgetPassword: {
        color: '#fff',
        textAlign: 'right',
        marginBottom: 20
    },
    newAccount: {
        color: '#fff',
        textAlign: 'right',
        marginBottom: 20
    },
    input: {
        width: 300,
        borderWidth: 1,
        borderRadius: 10,
        padding: 12,
        marginVertical: 10,
        backgroundColor: '#fff',
        borderColor: '#fff'
    },
    tombol: {
        width: 300,
        borderWidth: 1,
        borderRadius: 10,
        paddingVertical: 12,
        marginVertical: 5,
        backgroundColor: '#fff',
        borderColor: '#fff'
    },
    tombolText: {
        textAlign: 'center',
        color: '#318a83',
        fontWeight: 'bold',
        fontSize: 18
    },
    errorText: {
        color: 'white',
        textAlign: 'center',
        marginBottom: 16,
        fontWeight: 'bold'
    },
    hiddenErrorText: {
        color: 'transparent',
        textAlign: 'center',
        marginBottom: 16,
    }
})


