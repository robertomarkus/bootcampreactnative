import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, ScrollView, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class Login extends Component {
    render() {
        return (
            <View style={styles.container}>
                
            <View style={styles.navBar}>
                    {/* <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                        <Image source={require('./images/arrow_back.png')} style={{ width: 22, height: 22 }} />
                    </TouchableOpacity> */}
                    <Text style={styles.textNavbar}>Informasi Kesehatan</Text>
                </View>

        <ScrollView style={styles.body}>
          
          <Text style={{ color: '#4f4f4f', fontWeight: 'bold', fontSize: 20, textAlign: 'center', paddingTop: 20 }}>Tips Kesehatan dikala Pandemi</Text>
          <TouchableOpacity>
          <View style={styles.jumbotron}>            
            <View style={{ width: 150, height: 60, marginTop: 20, marginBottom: 30 }}>
              <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 18, marginBottom: 5 }}>Mutasi Virus <Icon name='coronavirus' size={20} /></Text>
              <Text style={{ color: '#fff', fontWeight: 'bold' }}>1. Varian Baru Corona</Text>
              <Text style={{ color: '#fff', fontWeight: 'bold' }}>2. Virus yang bermutasi</Text>
              <Text style={{ color: '#fff', fontWeight: 'bold' }}>3. 2x lebih berbahaya</Text>
            </View>
            <Image source={require('./images/virus.png')}
              style={{ width: 100, height: 100, marginTop: 20 }}
            />
        </View></TouchableOpacity>
        <TouchableOpacity>
          <View style={styles.jumbotron}>            
            <View style={{ width: 150, height: 60, marginTop: 20, marginBottom: 30 }}>
              <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 18, marginBottom: 5 }}>Masker Wajah <Icon name='masks' size={20} /></Text>
              <Text style={{ color: '#fff', fontWeight: 'bold' }}>1. Lapisan lebih</Text>
              <Text style={{ color: '#fff', fontWeight: 'bold' }}>2. Nyaman dipakai</Text>
              <Text style={{ color: '#fff', fontWeight: 'bold' }}>3. Bahan yang cocok</Text>
            </View>
            <Image source={require('./images/masker.png')}
              style={{ width: 100, height: 100, marginTop: 20 }}
            />
        </View></TouchableOpacity>
        <TouchableOpacity>
          <View style={styles.jumbotron}>            
            <View style={{ width: 150, height: 60, marginTop: 20, marginBottom: 30 }}>
              <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 18, marginBottom: 5 }}>Hand Sanitizer <Icon name='clean-hands' size={18} /></Text>
              <Text style={{ color: '#fff', fontWeight: 'bold' }}>1. Wajib Digunakan</Text>
              <Text style={{ color: '#fff', fontWeight: 'bold' }}>2. Ber-alkohol</Text>
              <Text style={{ color: '#fff', fontWeight: 'bold' }}>3. Selalu dibawa</Text>
            </View>
            <Image source={require('./images/sanitizer.png')}
              style={{ width: 80, height: 100, marginTop: 12 }}
            />
        </View></TouchableOpacity>
        <TouchableOpacity>
          <View style={styles.jumbotron}>            
            <View style={{ width: 150, height: 60, marginTop: 20, marginBottom: 30 }}>
              <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 18, marginBottom: 5 }}>Batasi Jarak <Icon name='6-ft-apart' size={22} /></Text>
              <Text style={{ color: '#fff', fontWeight: 'bold' }}>1. Minimal 2 Meter</Text>
              <Text style={{ color: '#fff', fontWeight: 'bold' }}>2. Bicara Seperlunya</Text>
              <Text style={{ color: '#fff', fontWeight: 'bold' }}>3. Hindari Keramaian</Text>
            </View>
            <Image source={require('./images/distancing.png')}
              style={{ width: 110, height: 100, marginTop: 15 }}
            />
        </View></TouchableOpacity>
        <TouchableOpacity>
          <View style={styles.jumbotron}>            
            <View style={{ width: 150, height: 60, marginTop: 20, marginBottom: 30 }}>
              <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 18, marginBottom: 5 }}>Makanan Sehat <Icon name='star' size={18} /></Text>
              <Text style={{ color: '#fff', fontWeight: 'bold' }}>1. Sayuran Hijau</Text>
              <Text style={{ color: '#fff', fontWeight: 'bold' }}>2. Buah Berserat</Text>
              <Text style={{ color: '#fff', fontWeight: 'bold' }}>3. Protein Secukupnya</Text>
            </View>
            <Image source={require('./images/food.png')}
              style={{ width: 110, height: 110, marginTop: 20 }}
            />
        </View></TouchableOpacity>
        <TouchableOpacity>
          <View style={styles.jumbotron}>            
            <View style={{ width: 150, height: 60, marginTop: 20, marginBottom: 30 }}>
              <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 18, marginBottom: 5 }}>Rutin Olahraga <Icon name='accessibility' size={18} /></Text>
              <Text style={{ color: '#fff', fontWeight: 'bold' }}>1. Jaga Pola Tidur!</Text>
              <Text style={{ color: '#fff', fontWeight: 'bold' }}>2. Makan yang Bergizi!</Text>
              <Text style={{ color: '#fff', fontWeight: 'bold' }}>3. Berolahraga Teratur!</Text>
            </View>
            <Image source={require('./images/exercize.png')}
              style={{ width: 100, height: 100, marginTop: 20 }}
            />
        </View></TouchableOpacity>
         
        </ScrollView>
         <View style={styles.tabBar}>
                    <TouchableOpacity
                        style={styles.tabItem}
                        onPress={() => this.props.navigation.navigate('Home')}>
                        <Icon name='home' size={25} style={{ color: '#ffff'}}/>
                        <Text style={styles.tabTitle}>Beranda</Text>
                    </TouchableOpacity>
                    <TouchableOpacity 
                    style={styles.tabItem}
                    onPress={() => this.props.navigation.navigate('News')}>
                        <Icon name='article' size={25} style={{ color: '#ffff'}}/>
                        <Text style={styles.tabTitle}>Tips</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.tabItem}
                        onPress={() => this.props.navigation.navigate('About')}>
                       <Icon name='list-alt' size={25} style={{ color: '#ffff', transform: [{ rotate: '180deg' }] }}/>
                        <Text style={styles.tabTitle}>Informasi
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.tabItem}
                        onPress={() => this.props.navigation.navigate('Profile')}>
                        <Icon name='miscellaneous-services' size={25} style={{ color: '#ffff'}}/>
                        <Text style={styles.tabTitle}>Pengaturan</Text>
                    </TouchableOpacity>
        </View>
      </View>



    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffff',
    textAlign: 'center'
  },
  body: {
    margin: 10,
  },
    navBar: {
        height: 84,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        backgroundColor: '#00ebab',
        elevation: 3,
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center',
    },
    textNavbar: {
        color: '#ffff',
        marginLeft: 85,
        fontWeight: 'bold',
        fontSize: 18,
    },
  jumbotron: {
    backgroundColor: '#00ebab',
    height: 120,
    borderRadius: 15,
    marginTop: 13,
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  dataContainer: {
    marginTop: 15,
    borderColor: '#4f4f4f',
    justifyContent: 'space-around',
    flexDirection: 'row',
    flex: 1,
    flexWrap: 'wrap',
    textAlign: 'center'
  },
  dataCovid: {
    width: 160,
    height: 80,
    borderRadius: 15,
    borderWidth: 0,
    borderColor: '#4f4f4f',
    justifyContent: 'center',
    textAlign: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    elevation: 3,
    marginBottom: 10,
  },
  tabBar: {
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: '#00ebab',
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-around'
  }, tabItem: {
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    color: '#858585'
  }, tabTitle: {
    fontSize: 11,
    color: '#ffff',
    paddingTop: 4
  }
})


