import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Ionicons } from '@expo/vector-icons';
import { ScrollView } from 'react-native-gesture-handler';

export default class Login extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.navBar}>
                    {/* <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                        <Image source={require('./images/arrow_back.png')} style={{ width: 22, height: 22 }} />
                    </TouchableOpacity> */}
                    <Text style={styles.textNavbar}>Tentang Aplikasi</Text>
                </View>
                <ScrollView>
                <View style={styles.mainDescription}>
                    <Text style={styles.pageTitle}>Hi There!</Text>
                    <Image source={require('./images/me.jpg')} style={{ width: 200, height: 200 }} />
                    <Text style={{ fontWeight: 'bold', fontSize: 24, borderBottomWidth: 1, borderBottomColor: '#4f4f4f' }}>Markus Roberto</Text>
                    <Text style={{ fontWeight: 'bold', fontSize: 16 }}>React Native Developer</Text>
                </View>
                <View style={styles.activities}>
                    <TouchableOpacity style={styles.listActivities}>
                        <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#00ebab' }}>22</Text>
                        <Text style={{ fontSize: 12, color: '#00ebab' }}>Certificates</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.listActivities}>
                        <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#00ebab' }}>17</Text>
                        <Text style={{ fontSize: 12, color: '#00ebab' }}>Experiences</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.listActivities}>
                        <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#00ebab' }}>28</Text>
                        <Text style={{ fontSize: 12, color: '#00ebab' }}>Projects</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.listActivities}>
                        <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#00ebab' }}>69</Text>
                        <Text style={{ fontSize: 12, color: '#00ebab' }}>Connections</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.mainDescription}>
                    <TouchableOpacity style={styles.button}>
                        <Text style={{ fontWeight: 'bold', color: 'white' }}>Skills Summary</Text>
                    </TouchableOpacity>
                </View>
                <View style={{ paddingHorizontal: 45, paddingVertical: 10 }}>
                    <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#4f4f4f', paddingBottom: 10 }}>External Portfolio :</Text>
                    <View style={styles.contentBox}>
                        <TouchableOpacity style={styles.boxItem}>
                            <Ionicons name="logo-bitbucket" size={30} color="#ffff" />
                            <Text style={{ fontSize: 10, fontWeight: 'bold', color: '#ffff' }}>@bitbucket</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.boxItem}>
                            <Ionicons name="logo-github" size={30} color="#ffff" />
                            <Text style={{ fontSize: 10, fontWeight: 'bold', color: '#ffff' }}>@github</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.boxItem}>
                            <Ionicons name="logo-dribbble" size={30} color="#ffff" />
                            <Text style={{ fontSize: 10, fontWeight: 'bold', color: '#ffff' }}>@dribbble</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.boxItem}>
                            <Ionicons name="logo-instagram" size={30} color="#ffff" />
                            <Text style={{ fontSize: 10, fontWeight: 'bold', color: '#ffff' }}>@instagram</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ paddingHorizontal: 45, paddingVertical: 10 }}>
                    <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#4f4f4f', paddingBottom: 10  }}>Contact Me :</Text>
                    <View style={styles.contentBox}>
                        <TouchableOpacity style={styles.boxItem}>
                            <Ionicons name="logo-linkedin" size={30} color="#ffff" />
                            <Text style={{ fontSize: 8, fontWeight: 'bold', color: '#ffff' }}>@linkedin</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.boxItem}>
                            <Ionicons name="mail-outline" size={30} color="#ffff" />
                            <Text style={{ fontSize: 8, fontWeight: 'bold', color: '#ffff' }}>google@gmail.com</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.boxItem}>
                            <Ionicons name="logo-whatsapp" size={30} color="#ffff" />
                            <Text style={{ fontSize: 8, fontWeight: 'bold', color: '#ffff' }}>+19483458457</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.boxItem}>
                            <Ionicons name="logo-facebook" size={30} color="#ffff" />
                            <Text style={{ fontSize: 8, fontWeight: 'bold', color: '#ffff' }}>@facebook</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
            <Text>{"\n"}</Text>
                <View style={styles.tabBar}>
                    <TouchableOpacity
                        style={styles.tabItem}
                        onPress={() => this.props.navigation.navigate('Home')}>
                        <Icon name='home' size={25}  style={{ color: '#ffff'}}/>
                        <Text style={styles.tabTitle}>Beranda</Text>
                    </TouchableOpacity>
                    <TouchableOpacity 
                    style={styles.tabItem}
                    onPress={() => this.props.navigation.navigate('News')}>
                        <Icon name='article' size={25}  style={{ color: '#ffff'}}/>
                        <Text style={styles.tabTitle}>Tips</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.tabItem}
                        onPress={() => this.props.navigation.navigate('About')}>
                        <Icon name='list-alt' size={25} style={{ color: '#ffff', transform: [{ rotate: '180deg' }] }}/>
                        <Text style={styles.tabTitle}>Informasi</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.tabItem}
                        onPress={() => this.props.navigation.navigate('Profile')}>
                        <Icon name='miscellaneous-services' size={25} style={{ color: '#ffff'}}/>
                        <Text style={styles.tabTitle}>Pengaturan</Text>
                    </TouchableOpacity>
                </View>
            </View>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffff'
    },
    navBar: {
        height: 84,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        backgroundColor: '#00ebab',
        elevation: 3,
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center',
    },
    textNavbar: {
        color: '#ffff',
        marginLeft: 60,
        fontWeight: 'bold',
        fontSize: 18,
        paddingLeft: 40
    },
    body: {
        flex: 1,
        marginTop: 20,
        alignItems: 'center',
        textAlign: 'center'
    },
    aboutApp: {
        fontSize: 15,
        margin: 20,
        padding: 24,
        backgroundColor: '#00ebab',
        color: '#fff',
        borderRadius: 15,
        textAlign: 'justify'
    },
    tabBar: {
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: '#00ebab',
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-around'
  }, tabItem: {
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    color: '#858ff585'
  }, tabTitle: {
    fontSize: 11,
    color: '#ffff',
    paddingTop: 4
  },



  
    pageTitle: {
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: 30,
        fontSize: 25,
        fontWeight: 'normal',
        paddingBottom: 10
    },
    mainDescription: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    activities: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 10
    },
    listActivities: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 5,
    },
    button: {
        flexDirection: 'row',
        backgroundColor: '#00ebab',
        alignItems: 'center',
        justifyContent: 'center',
        height: 40,
        width: 160,
        borderRadius: 20,
        marginLeft: 50,
        marginRight: 50,
        marginTop: 10,
        marginBottom: 10
    },
    contentBox: {
        flexDirection: 'row',
        backgroundColor: '#00ebab',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 10,
        paddingHorizontal: 10,
        borderRadius: 15
    },
    boxItem: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 5,
    }
})


