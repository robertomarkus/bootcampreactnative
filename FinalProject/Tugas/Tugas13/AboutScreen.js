import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';

export default class App extends Component {

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.navBar}>
                    <TouchableOpacity >
                        <Image source={require('./images/arrow_back.png')} style={{ color: 'white' }} />
                    </TouchableOpacity>
                    <Text style={{ color: 'white', marginLeft: 75, fontWeight: 'bold', fontSize: 22 }}>USER ABOUT</Text>
                </View>
                <View style={styles.body}>
                    <View>
                        <Image source={require('./images/user.png')} style={{ width: 150, height: 150 }} />
                        <Text style={styles.nameText}>Fadholi Rahman</Text>
                    </View>
                    <View style={{ marginBottom: 45 }}>
                        <Text style={styles.textInfo}>Portfolio</Text>
                        <View>
                            <Image source={require('./images/github.png')} style={{ width: 30, height: 30, alignSelf: 'center' }} />
                        </View>
                    </View>
                    <View>
                        <Text style={styles.textInfo}>Social Media</Text>
                        <View style={styles.sosialMedia}>
                            <Image source={require('./images/facebook.png')} style={{ width: 30, height: 30 }} />
                            <Image source={require('./images/instagram.png')} style={{ width: 30, height: 30, marginLeft: 25, marginRight: 25 }} />
                            <Image source={require('./images/twitter.png')} style={{ width: 30, height: 30 }} />
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    navBar: {
        height: 60,
        backgroundColor: '#02157B',
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 50
    },
    body: {
        alignItems: 'center',
    },
    nameText: {
        fontSize: 20,
        fontWeight: 'bold',
        margin: 'auto',
        textAlign: 'center',
        marginTop: 15,
        marginBottom: 35,
    },
    textInfo: {
        color: '#02157B',
        fontSize: 22,
        fontWeight: 'bold',
        margin: 'auto',
        textAlign: 'center',
        marginBottom: 10
    },
    sosialMedia: {
        flexDirection: 'row',
    }
})